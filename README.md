# Remembering the Kanji

> This application is based on the book **Remembering the Kanji** written by
_James W. Heisig_. It will guide one on the path to remember and recall the
kanji written form from memory with keywords and little stories.

## How to build & run the application

### Install Flutter

You can follow the [official documentation](https://docs.flutter.dev/get-started/install)
to install flutter on your machine.

Once the installation is complete you can run the `flutter doctor` command
to make sure everything is up to date with your installation.

### Clone this repository & Install dependencies

```sh
# Clone the repository
git clone git@gitlab.com:nlaforet/laforet.apps.rtk.git
# Move to the project directory
cd laforet.apps.rtk

# Install project dependencies
flutter pub get
```

### Run the application

#### **Web**

```sh
# Inside the project root directory run the following command:
flutter run -d chrome
```

#### **Android**

```sh
# Inside the project root directory run the following command:
flutter run -d '<android device name>'
```

#### **iOS** - Only available on macOS

```sh
# Inside the project root directory run the following command:
flutter run -d '<iOS device name>'
```
